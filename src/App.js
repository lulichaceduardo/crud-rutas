import React from 'react';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import Header from './components/Header';
import Productos from './components/Productos';
import AgregarProducto from './components/AgregarProducto';
import EditarProducto from './components/EditarProducto';

class App extends React.Component {
  render() {
    return (
      <Router>
        <Header/>
        <main className="container">
          <Switch>
            <Route exact path="/productos/nuevo" component ={AgregarProducto}/>
            <Route exact path="/">
              <Redirect to="/productos"></Redirect>
            </Route>
            <Route exact path="/productos" component ={Productos}/>
            <Route exact path="/productos/editar/:id" component={EditarProducto} />
          </Switch>
        </main>
      </Router>
    );
  }
}

export default App;
