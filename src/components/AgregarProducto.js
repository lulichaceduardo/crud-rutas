import React from 'react';
import Form from './Form'
import {withRouter} from 'react-router-dom'

class AgregarProducto extends React.Component {
  render() {
    return (
      <Form reloadProduct={this.props.reloadProduct}/>
    );
  }
  
};

export default withRouter(AgregarProducto);