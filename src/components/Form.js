import React, { Component } from 'react';
import Error from './Error';
import {withRouter} from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2'

const url = 'http://localhost:4000/restaurant';

class Form extends Component {
  state = {
    producto: {
      nombrePlatillo: '',
      precioPlatillo: '',
      categoria: '',
    },
    error: false
  }
  componentDidMount() {
    const {option} = this.props;
    if(option === 'edit'){
      this.setState({producto: this.props.producto})
    }
  }
  handleChange = (e)=>{
    this.setState({
      producto: {
        ...this.state.producto,
        [e.target.name]: e.target.value
      }
    })
  }
  handleSubmit = async (e) => {
    e.preventDefault();
    const {nombrePlatillo, precioPlatillo, categoria} = this.state.producto
    if(nombrePlatillo === '' || precioPlatillo === '' || categoria === ''){
      this.setState({error: true})
      return;
    }
    this.setState({error: false});

    // Ejecutar evento de guardado o actualización
    const {option} = this.props;
    let urlLocal;
    if(option === 'edit'){
      const {id} = this.state.producto
      urlLocal = `${url}/${id}`
    } else {
      urlLocal = url
    }
    const {producto} = this.state
    try {
      let resultado;
        (option === 'edit') ? 
          resultado = await axios.put(urlLocal,{...producto}):
          resultado = await axios.post(urlLocal,{...producto}); 
      if(resultado.status === 201 || resultado.status === 200) {
        this.alerta();
        this.props.history.push('/productos');
      }
    } catch (error) {
      console.log(error);
    }
  }

  alerta = () => {
    const {option} = this.props;
    Swal.fire(
      'Exito!',
      (option === 'edit')?
      'Producto Editado corectamente': 'Producto agregado corectamente',
      'success'
    )
  }
  render() {
    const {error} = this.state;
    return (
      <div>
        <h1 className="text-center">Agregar Nuevo producto</h1>
        {error && <Error mensaje="Todos los campos son obligatorios"/>}
        <form className="mt-4" onSubmit={this.handleSubmit}>
          <div className="form-row justify-content-center">
            <div className="form-group col-md-6">
              <label htmlFor="inputEmail4">Nombre</label>
              <input 
                type="text" 
                className="form-control" 
                name="nombrePlatillo"
                onChange= {this.handleChange}
                value= {this.state.producto.nombrePlatillo}
                placeholder="Nombre del platillo"/>
            </div>
          </div>
          <div className="form-row justify-content-center">
            <div className="form-group col-md-6">
              <label htmlFor="inputAddress">Precio</label>
              <input 
                type="number" 
                className="form-control"
                name="precioPlatillo"
                onChange= {this.handleChange}
                value= {this.state.producto.precioPlatillo}
                placeholder="Precio"/>
            </div>
          </div>
          <div className="form-row justify-content-center mt-3">
            <div className="form-check form-check-inline">
              <input 
                className="form-check-input" 
                type="radio" 
                name="categoria" 
                onChange= {this.handleChange}
                checked = {this.state.producto.categoria === 'Postres'}
                value="Postres" />
              <label 
                className="form-check-label" 
                htmlFor="Postres">Postres</label>
            </div>
            <div className="form-check form-check-inline">
              <input 
                className="form-check-input" 
                type="radio" 
                name="categoria" 
                onChange= {this.handleChange}
                checked = {this.state.producto.categoria === 'Bebidas'}
                value="Bebidas" />
              <label 
                className="form-check-label" 
                htmlFor="Bebidas">Bebidas</label>
            </div>
            <div className="form-check form-check-inline">
              <input 
                className="form-check-input" 
                type="radio" 
                name="categoria" 
                onChange= {this.handleChange}
                checked = {this.state.producto.categoria === 'Cortes'}
                value="Cortes" />
              <label 
                className="form-check-label" 
                htmlFor="Cortes">Cortes</label>
            </div>
            <div className="form-check form-check-inline">
              <input 
                className="form-check-input" 
                type="radio" 
                name="categoria" 
                onChange= {this.handleChange}
                checked = {this.state.producto.categoria === 'Ensaladas'}
                value="Ensaladas" />
              <label 
                className="form-check-label" 
                htmlFor="Ensaladas">Ensaladas</label>
            </div>
          </div>
          <div className="form-row justify-content-center mt-4">
            <button  type="submit" className="btn btn-primary col-md-6">Agregar</button>
          </div>
        </form>
      </div>
    );
  }
}

export default withRouter(Form);