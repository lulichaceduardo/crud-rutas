import React from 'react';
import {Link, NavLink} from 'react-router-dom'
const Header = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <Link 
        to="/Productos" 
        className="navbar-brand" >React Router</Link>
      <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
          <li className="nav-item">
            <NavLink 
              className="nav-link" 
              activeClassName="active"
              exact
              to="/Productos">Productos </NavLink>
          </li>
          <li className="nav-item">
            <NavLink 
              className="nav-link" 
              activeClassName="active"
              exact
              to="/productos/nuevo">Agregar Productos</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Header;