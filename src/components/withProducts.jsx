import React from 'react';
import axios from 'axios';
const withProducts = Component => {

  class WithProducts extends React.Component {
    state = {
      listaProductos: [],
    }
    componentDidMount() {
      this.getProductos();
    }
    getProductos = async ()=> {
      const resultado = await axios.get('http://localhost:4000/restaurant');
      this.setState({listaProductos:resultado.data})
    }
    getProducto = (id) => {
      const idProducto = parseInt(id);
      const producto = this.state.listaProductos.filter(producto=> producto.id === idProducto);
      return producto[0];
    }

    render() {
      return <Component 
                listaProductos={this.state.listaProductos} 
                reloadProduct = {this.getProductos}
                editProduct = {this.getProducto}
                {...this.props}/>
    }
  }

  return WithProducts;

}

export default withProducts;