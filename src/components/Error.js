import React from 'react';

const Error = ({mensaje}) => {
  return (
    <div className="row justify-content-center">
      <p 
        className="alert col-md-6 alert-danger p3 my-2 text-center text-uppercase">
          {mensaje}</p>
    </div>
  );
};

export default Error;