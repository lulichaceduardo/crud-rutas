import React from 'react';
import Form from './Form'
import {withRouter} from 'react-router-dom'
import withProducts from './withProducts';

class EditarProducto extends React.Component{
  render(){
    console.log('mira propiedades');
    console.log(this.props);
    const id = this.props.match.params.id;
    const producto = this.props.editProduct(id);
    console.log("el producto es");
    console.log(producto);
    if(producto){
      return (
        <Form
          producto={producto}
          option="edit"/>
      )
    } else {
        return <h1>Cargando</h1>
    }
    
  }
};

export default withProducts(withRouter(EditarProducto));